#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

int main(int argc, char **argv){
	if(argc!=3){
		printf("Argumentos inválidos, revise que sean rutas o archivos");
		return -1;
	}
	char buffer[10000]={0};
	umask(0);
	int origen=open(argv[1],O_RDONLY,0666);
	int destino=open(argv[2],O_TRUNC | O_CREAT | O_RDWR,0666);
	if(origen<0){
		perror("Ruta o archivo no encontrado");
	}
	int bytesleidos;
	int bytescopiados;
	int tbytes=0;

	while((bytesleidos=read(origen,buffer,10000))!=0){
		if (bytesleidos < 0){
		
		  perror("ERROR AL LEER EL ARCHIVO");
		
		}

		bytescopiados = write(destino, buffer, bytesleidos);

		if(bytescopiados < 0){
		  perror("ERROR AL ESCRIBIR EN EL ARCHIVO DESTINO");
		
		
		}

		tbytes = tbytes + bytescopiados;

	}

	close(origen);
	close(destino);

	if(tbytes > 0){
	   printf("%d bytes copiados \n", tbytes);

	
	
	
	}

	else{
	  printf("Se produjo errores");
	
	
	
	}

	return 0;
}







